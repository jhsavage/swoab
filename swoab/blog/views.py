from django.views.generic import ListView, DetailView, CreateView, DeleteView, TemplateView
from .models import Post, Comment, Profile, Likes, ControlPanel
from .forms import CommentForm
from django.shortcuts import render, get_object_or_404, reverse
from .forms import ContactForm
from django.core.mail import send_mail
from django.shortcuts import render, redirect
from django.views.generic.edit import FormView
from django.contrib.auth.forms import UserCreationForm
from django.views import generic
from django.db.models import Count
from django.db.models import Q
from django.contrib.auth.models import User
from django.contrib.auth import logout


class BlogListView(ListView):
    model = Post
    template_name = 'home.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['num_of_approved'] = Comment.objects.filter(approved_comment=True)  # total num of approved
        context['object_list'] = Post.objects.annotate(
            number=Count('comments', filter=Q(comments__approved_comment=True))
        )
        return context


class BlogDetailView(DetailView):
    model = Post
    template_name = 'post_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['comment_list'] = Comment.objects.filter(approved_comment=True)
        context['form'] = CommentForm()
        return context

    def post(self, request, *args, **kwargs):
        like = request.POST.get('like')
        delete_button = request.POST.get('delete')

        if like:
            self.object = self.get_object()
            self.object.likes.add(request.user)
            self.object.save()
            return render(request, "post_detail.html", self.get_context_data())

class CommentCreateView(CreateView):
    model = Comment
    form = CommentForm
    fields = ['text']

    def form_valid(self, form):
        form.instance.post_id = self.kwargs.get('pk')
        form.instance.author = self.request.user
        return super(CommentCreateView, self).form_valid(form)

    def get_success_url(self):
        return reverse('post_detail', kwargs={"pk": self.kwargs.get('pk')})


class EmailView(FormView):
    template_name = 'email.html'
    form_class = ContactForm
    success_url = '/email_sent/'

    def form_valid(self, form):
        subject = form.cleaned_data['subject']
        from_email = form.cleaned_data['from_email']
        message = form.cleaned_data['message']
        ver_email = form.cleaned_data['ver_email']
        if ver_email == from_email:
            if send_mail(subject, message + '\n\nfrom ' + from_email, 'jordan@idxtra.com', ['jordan@idxtra.com']):
                print('email sent')
                return super().form_valid(form)
        return redirect('/email_not_sent/')


class SignUp(generic.CreateView):
    form_class = UserCreationForm
    success_url = '/accounts/login'
    template_name = 'signup.html'

    def form_valid(self, form):
        print('form saved')
        form.save()
        return super(SignUp, self).form_valid(form)


class Profile(TemplateView):
    model = Profile
    template_name = "profile.html"

    def get_context_data(self, **kwargs):
        if self.request.user.is_authenticated:
            context = super().get_context_data(**kwargs)
            context['overall_comments'] = Comment.objects.filter(
                approved_comment=True,
                author=self.request.user
            ).count()  # total number of approved for that specific user

            context['posts_liked'] = Post.objects.filter(
                likes=self.request.user
            ).count()  # total number of posts that specific user has liked

            # get all the titles of the posts they have liked

            # get all the titles of the posts they have commented on

            # display blog title where user has liked OR commented

            context['blog_titles'] = Post.objects.filter(
                Q(likes=self.request.user) | Q(comments__author=self.request.user, comments__approved_comment=True)
            ).distinct()  # posts the user has interacted with

            context['liked_post_titles'] = Post.objects.filter(
                Q(likes=self.request.user)
            ).distinct()

            context['commented_on_post_titles'] = Post.objects.filter(
                Q(comments__author=self.request.user, comments__approved_comment=True)
            ).distinct()

            context['total_num_of_interactions'] = int(context['overall_comments']) + int(context['posts_liked'])
            return context


class ControlPanel(TemplateView):
    model = ControlPanel
    template_name = "control_panel.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['object_list'] = Post.objects.all()
        context['user_list'] = User.objects.filter(is_staff=False)
        context['staff_list'] = User.objects.filter(is_staff=True)
        return context

    def post(self, request, *args, **kwargs):
        delete_button = request.POST.get('delete')
        delete_user_button = request.POST.get('delete-user')

        if delete_button:
            Post.objects.filter(id=request.POST['id']).delete()
            return render(request, "control_panel.html", self.get_context_data())

        if delete_user_button:
            User.objects.filter(id=request.POST['userId']).delete()
            return render(request, "control_panel.html", self.get_context_data())

        else:
            return render(request, "control_panel.html", self.get_context_data())
