from django.utils import timezone
from django.utils.safestring import mark_safe
from django.forms import ModelForm
from django.db import models
from django.contrib.auth.hashers import *
from django.contrib import admin


class Post(models.Model):
    author = models.ForeignKey(
        'auth.User',
        on_delete=models.CASCADE,
        related_name='author',
    )
    title = models.CharField(max_length=200)
    text = models.TextField()
    image = models.ImageField(null=True, blank=True, upload_to="images/img/")
    likes = models.ManyToManyField('auth.User', related_name='posts', null=True, blank=True)
    tags = models.TextField(blank=True)
    created_date = models.DateTimeField(default=timezone.now)

    def headshot_image(self, obj):
        return mark_safe('<img src="{url}" width="{width}" height={height} />'.format(
            url=obj.image.url,
            width=obj.image.width,
            height=obj.image.height,
        ))

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['-created_date', ]


class PostAdmin(admin.ModelAdmin):
    exclude = ('likes',)


class Comment(models.Model):
    post = models.ForeignKey(
        'blog.post',
        on_delete=models.CASCADE,
        related_name='comments'
    )
    author = models.ForeignKey(
        'auth.User',
        on_delete=models.CASCADE,
        related_name='author_comment',
        null=True,
        blank=True
    )
    text = models.TextField()
    created_date = models.DateTimeField(default=timezone.now)
    approved_comment = models.BooleanField(default=True)

    def approve(self, *args, **kwargs):
        self.approved_comment = True

    def __str__(self):
        return self.text + ' | ' + str(self.author)

    class Meta:
        ordering = ['-created_date', ]


class UserComment(ModelForm):

    class Meta:
        model = Comment
        fields = ['text']


class Likes(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name='+')
    value = models.IntegerField()
    date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.user) + ':' + str(self.post) + ':' + str(self.value)

    class Meta:
        unique_together = ("post", "value")
        ordering = ['-date', ]


class Profile(models.Model):
    pubComments = models.ForeignKey(
        'blog.comment',
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )
    likedPosts = models.ForeignKey(
        'blog.likes',
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )


class ControlPanel(models.Model):
    comments = models.ForeignKey(
        'blog.comment',
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )
