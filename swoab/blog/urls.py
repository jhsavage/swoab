from django.urls import path, re_path
from django.conf.urls import url
from . import views
from .views import EmailView
from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required, permission_required


urlpatterns = [
    path('', views.BlogListView.as_view(), name='home'),
    path('post/<int:pk>/', views.BlogDetailView.as_view(), name='post_detail'),
    path('post/<int:pk>/comment/', views.CommentCreateView.as_view(), name='comment_detail'),
    path('email/', EmailView.as_view(), name='email'),
    path('email_sent/', TemplateView.as_view(template_name='email_sent.html')),
    path('email_not_sent/', TemplateView.as_view(template_name='email_not_sent.html')),
    path('signup/', views.SignUp.as_view(), name='signup'),
    path('profile/', views.Profile.as_view(), name='profile'),
    path('control_panel/', login_required(views.ControlPanel.as_view()), name='control_panel'),
]
