from .models import Post, Comment
from django import forms


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ['author', 'text']


class ContactForm(forms.Form):
    from_email = forms.EmailField(widget=forms.TextInput(attrs={'placeholder': 'name@example.com'}), required=True)
    ver_email = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'name@example.com'}), required=True)
    subject = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Subject'}), required=True)
    message = forms.CharField(widget=forms.Textarea(attrs={'placeholder': 'Message'}), required=True)

